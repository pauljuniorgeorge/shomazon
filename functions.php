<?php

function is_user(){
    return isset($_COOKIE["access_token"]);
}

function ajax_request(){
    return isset($_POST["ajax"]);
}

function response($status="false",$message="No Message Provided", $payload = null){
    echo json_encode(["status" => $status, "message" => $message, "payload" => json_encode($payload)]);
    exit();
}

function is_imported($shop){
    global $db;
    $db->where("domain",$shop);
    $shop = $db->get("shops");

    $db->where("shop_id",$shop[0]["shop_id"]);
    $products = $db->get("products");

    return (empty($products)) ? false : true;
}

function is_paid($shop){
    global $db;
    $db->where("domain",$shop);
    $shop = $db->get("shops");
    $now = time();
    $str = $now - strtotime($shop[0]['last_payment_date']);
    $days = round($str / (60 * 60 * 24));
    return  ($days < 30) ? true : false;
}

function addProduct($product,$shopDomain){

    global $db;

    //Check for header if not check for cookie if not return error
    if (isset($_COOKIE["shop"])){
        $shopDomain = $_COOKIE["shop"];
    }

    $db->where("domain",$shopDomain);
    $shop = $db->get("shops");

    $db->insert('products',
        array(
            "product_id" => $product['id'],
            "body_html" => ($product["body_html"] == NULL) ? " " : $product["body_html"],
            "title" => $product["title"],
            "handle" => $product["handle"],
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => $product["updated_at"],
            "shop_id" => $shop[0]["shop_id"],
            "active" => 1
        ));

error_log($db->getLastError());


 $db->where("vendor_name", $product["vendor"]);
    $vendorDB = $db->getOne("vendor_names");

    if(empty($vendorDB)){
        $id = $db->insert('vendor_names',
            array(
                "vendor_name" => $product["vendor"]
            ));
    }
    else{
        $id = $vendorDB["id"];
    }

    $db->insert('vendors',
        array(
            "name" => $product["vendor"],
            "product_id" => $product["id"],
             "vendor_name_id" => $id
        ));



    $db->where("name", $product["product_type"]);
    $typeDB = $db->getOne("types");

    if (empty($typeDB)) {
        $id = $db->insert("types", array(
            "name" => $product["product_type"]
        ));
    } else {
        $id = $typeDB["id"];
    }

    $db->insert('product_types', array(
        "type_id" => $id,
        "product_id" => $product["id"]
    ));



    $tagString = explode(",", $product["tags"]);
    foreach ($tagString as $tag) {
        $db->where("name", $tag);
        $tagDB = $db->getOne("tags");

        if (empty($tagDB)) {
            $id = $db->insert("tags", array(
                "name" => $tag
            ));
        } else {
            $id = $tagDB["id"];
        }

        $db->insert("product_tags", array(
            "product_id" => $product["id"],
            "tag_id" => $id
        ));


    }

//error_log("Before");

    foreach ($product["variants"] as $variants) {

//error_log("After");
        $db->where("variant_title", $variants["title"]);
        $variantTitleDB = $db->getOne("variant_titles");

        if(empty($variantTitleDB)){
         $id = $db->insert('variant_titles',
                array(
                    "variant_title" => $variants["title"]
                 ));
        }
        else{
            $id = $variantTitleDB["id"];
        }


        $db->insert("variants", array(
            "product_id" => $product["id"],
            "title" => $variants["title"],
            "title_id" => $id,
            "price" => $variants["price"],
            "sku" => $variants["sku"],
            "variant_id" => $variants["id"],
	    "inventory_item_id" => ($variants["inventory_item_id"] == NULL) ? 0 : $variants["inventory_item_id"]
        ));

//error_log("VARIANTS");
//error_log($db->getLastError());

    }

    foreach ($product["options"] as $option) {
        foreach ($option["values"] as $value) {
            $db->insert("options", array(
                "option_id" => $option["id"],
                "product_id" => $option["product_id"],
                "name" => $option["name"],
                "value" => $value
            ));
        }
    }

    foreach ($product["images"] as $image) {
        $db->insert("images", array(
            "product_id" => $product["id"],
            "width" => $image["width"],
            "height" => $image["height"],
            "src" => $image["src"]
        ));
    }

//error_log("END");
//error_log($db->getLastError());

}

function addShop($shop,$token=""){

    global $db;

    //Country
    $db->where("country_name", $shop["country_name"]);
    $countryNamesDB = $db->getOne("country_names");
    if (empty($countryNamesDB)) {
        $country_id = $db->insert("country_names", array(
            "country_name" => $shop["country_name"]
        ));
    } else {
        $country_id = $countryNamesDB["id"];
    }

    //Province
    $db->where("province_name", $shop["province"]);
    $provinceNamesDB = $db->getOne("province_names");
    if (empty($provinceNamesDB)) {
        $province_id = $db->insert("province_names", array(
            "province_name" => $shop["province"]
        ));
    } else {
        $province_id = $provinceNamesDB["id"];
    }

    //City
    $db->where("city_name", $shop["city"]);
    $cityNamesDB = $db->getOne("city_names");
    if (empty($cityNamesDB)) {
        $city_id = $db->insert("city_names", array(
            "city_name" => $shop["city"]
        ));
    } else {
        $city_id = $cityNamesDB["id"];
    }

    $db->where("shop_id",$shop["id"]);
    $db->delete("shops");


//Country
    $id = $db->insert('shops',
        array(
            "name" => $shop["name"],
            "email" => $shop["email"],
            "domain" => $shop["domain"],
            "province_id" => $province_id,
            "country_id" => $country_id,
            "city_id" => $city_id,
            "province" => $shop["province"],
            "country" => $shop["country"],
            "city" => $shop["city"],
            "country_code" => $shop["country_code"],
            "country_name" => $shop["country_name"],
            "customer_email" => $shop["customer_email"],
            "shop_owner" => $shop["shop_owner"],
            "shop_id" => $shop["id"],
            "access_token" => $token
        ));


    foreach ($shop['enabled_presentment_currencies'] as $currency) {
        $db->replace("currencies",
            array(
                "name" => $currency,
                "shop_id" => $shop['id']
            )
        );
    }

}

function addCollect($collect){

    global $db;
    global $shopify;

    $collectionAPI = $shopify->Collection($collect["collection_id"])->get();
    // Lookup Collection By Name
    $db->where("title", $collectionAPI["title"]);
    $collectionDB = $db->getOne("collections");

    if (empty($collectionDB)) {
        $id = $db->insert("collections", array(
            "title" => $collectionAPI["title"],
            "handle" => $collectionAPI["handle"]
        ));
    } else {
        $id = $collectionDB["id"];
    }

    $id = $db->insert("product_collections", array(
        "product_id" => $collect["product_id"],
        "collection_id" => $id
    ));
}

function deleteProduct($product_id){

	    global $db;

            $db->where ("product_id", $product_id, '=');
            $res = $db->delete("product_collections");
            $res = $db->delete("product_tags");
            $res = $db->delete("product_types");
            $res = $db->delete("images");
            $res = $db->delete("variants");

            $db->where ("product_id", $product_id, '=');
            $db->delete("products");
}

function updateShop($shop){

global $db;

    //Country
    $db->where("country_name", $shop["country_name"]);
    $countryNamesDB = $db->getOne("country_names");
    if (empty($countryNamesDB)) {
        $country_id = $db->insert("country_names", array(
            "country_name" => $shop["country_name"]
        ));
    } else {
        $country_id = $countryNamesDB["id"];
    }

    //Province
    $db->where("province_name", $shop["province"]);
    $provinceNamesDB = $db->getOne("province_names");
    if (empty($provinceNamesDB)) {
        $province_id = $db->insert("province_names", array(
            "province_name" => $shop["province"]
        ));
    } else {
        $province_id = $provinceNamesDB["id"];
    }

    //City
    $db->where("city_name", $shop["city"]);
    $cityNamesDB = $db->getOne("city_names");
    if (empty($cityNamesDB)) {
        $city_id = $db->insert("city_names", array(
            "city_name" => $shop["city"]
        ));
    } else {
        $city_id = $cityNamesDB["id"];
    }



//Country
	$db->where("id",$shop["id"]);
	$db->update('shops',
        array(
            "name" => $shop["name"],
            "email" => $shop["email"],
            "domain" => $shop["domain"],
            "province_id" => $province_id,
            "country_id" => $country_id,
            "city_id" => $city_id,
            "province" => $shop["province"],
            "country" => $shop["country"],
            "city" => $shop["city"],
            "country_code" => $shop["country_code"],
            "country_name" => $shop["country_name"],
            "customer_email" => $shop["customer_email"],
            "shop_owner" => $shop["shop_owner"],
            "shop_id" => $shop["id"]
        ));


    foreach ($shop['enabled_presentment_currencies'] as $currency) {
        $db->insert("currencies",
            array(
                "name" => $currency,
                "shop_id" => $shop['id']
            )
        );
    }


error_log("FINISHED UPDATE");
error_log($db->getLastError());
}

function uninstallApp($shop,$storeName){

global $db;	    
$db->where("domain",$storeName);
            $curShop = $db->get("shops");

            $ids = $db->subQuery();
            $ids->where ("shop_id", $curShop[0]['shop_id'], "=");
            $ids->get ("products", null, "product_id");

            $db->where ("product_id", $ids, 'in');
            $res = $db->delete("product_collections");
            $res = $db->delete("product_tags");
            $res = $db->delete("product_types");
            $res = $db->delete("images");
            $res = $db->delete("variants");

		$res = $db->delete("vendors");
            $db->where ("shop_id", $curShop[0]['shop_id'], '=');
            $db->delete("products");
	    $db->delete("shops");
            
}

function updateProduct($product,$storeName){

 global $db;

    //Check for header if not check for cookie if not return error

    $db->where("domain",$storeName);
    $shop = $db->get("shops");

     $db->where("product_id",$product["id"]);
     $db->update('products',
        array(
            "body_html" => ($product["body_html"] == NULL) ? " " : $product["body_html"],
            "title" => $product["title"],
            "handle" => $product["handle"],
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
            "shop_id" => $shop[0]["shop_id"],
            "active" => 1
        ));

error_log($db->getLastError());


 $db->where("vendor_name", $product["vendor"]);
    $vendorDB = $db->getOne("vendor_names");

    if(empty($vendorDB)){
        $id = $db->insert('vendor_names',
            array(
                "vendor_name" => $product["vendor"]
            ));
    }
    else{
        $id = $vendorDB["id"];
    }

    $db->where("product_id",$product["id"]);
    $db->delete("vendors");

    $db->insert('vendors',
        array(
            "name" => $product["vendor"],
            "product_id" => $product["id"],
             "vendor_name_id" => $id
        ));



    $db->where("name", $product["product_type"]);
    $typeDB = $db->getOne("types");

    if (empty($typeDB)) {
        $id = $db->insert("types", array(
            "name" => $product["product_type"]
        ));
    } else {
        $id = $typeDB["id"];
    }

 $db->where("product_id",$product["id"]);
$db->delete("product_types");    

$db->insert('product_types', array(
        "type_id" => $id,
        "product_id" => $product["id"]
    ));



$db->where("product_id",$product["id"]);
$db->delete("product_tags");

    $tagString = explode(",", $product["tags"]);
    foreach ($tagString as $tag) {
        $db->where("name", $tag);
        $tagDB = $db->getOne("tags");

        if (empty($tagDB)) {
            $id = $db->insert("tags", array(
                "name" => $tag
            ));
        } else {
            $id = $tagDB["id"];
        }

        $db->insert("product_tags", array(
            "product_id" => $product["id"],
            "tag_id" => $id
        ));


    }

//error_log("Before");

$db->where("product_id",$product["id"]);
$db->delete("variants");
    foreach ($product["variants"] as $variants) {

//error_log("After");
        $db->where("variant_title", $variants["title"]);
        $variantTitleDB = $db->getOne("variant_titles");

        if(empty($variantTitleDB)){
         $id = $db->insert('variant_titles',
                array(
                    "variant_title" => $variants["title"]
                 ));
        }
        else{
            $id = $variantTitleDB["id"];
        }



        $db->insert("variants", array(
            "product_id" => $product["id"],
            "title" => $variants["title"],
            "title_id" => $id,
            "price" => $variants["price"],
            "sku" => $variants["sku"],
            "variant_id" => $variants["id"],
	    "inventory_item_id" => ($variants["inventory_item_id"] == NULL) ? 0 : $variants["inventory_item_id"]
        ));

//error_log("VARIANTS");
//error_log($db->getLastError());

    }
$db->where("product_id", $product["id"]);
$db->delete("options");

    foreach ($product["options"] as $option) {
        foreach ($option["values"] as $value) {
            $db->insert("options", array(
                "option_id" => $option["id"],
                "product_id" => $option["product_id"],
                "name" => $option["name"],
                "value" => $value
            ));
        }
    }

$db->where("product_id", $product["id"]);
$db->delete("images");

    foreach ($product["images"] as $image) {
        $db->insert("images", array(
            "product_id" => $product["id"],
            "width" => $image["width"],
            "height" => $image["height"],
            "src" => $image["src"]
        ));
    }

//error_log("END");
//error_log($db->getLastError());

}//update product


?>

<?php
require 'vendor/autoload.php';
require 'config.php';
use LukeTowers\ShopifyPHP\Shopify;

function make_authorization_attempt($shop, $scopes)
{
    $api = new Shopify($shop, [
        'api_key' => API_KEY,
        'secret' => API_SECRET,
    ]);

    $nonce = bin2hex(random_bytes(10));

    // Store a record of the shop attempting to authenticate and the nonce provided
    $storedAttempts = file_get_contents('authattempts.json');
    $storedAttempts = $storedAttempts ? json_decode($storedAttempts) : [];
    $scopes = ["unauthenticated_read_product_listings","unauthenticated_read_product_tags","read_products"];
    $storedAttempts[] = ['shop' => $shop, 'nonce' => $nonce, 'scopes' => $scopes];
    file_put_contents('authattempts.json', json_encode($storedAttempts));

    return $api->getAuthorizeUrl($scopes, REDIRECT_URI, $nonce);
}

header('Location: ' . make_authorization_attempt($_POST['shop'], ['read_product']));
die();
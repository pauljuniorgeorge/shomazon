<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ProgressTracker extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
       $table = $this->table('progress_tracker');
        $table->addColumn('shop_id', 'biginteger')
            ->addColumn('total', 'biginteger')
            ->addColumn('last_progress_count', 'integer')
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `progress_tracker` CHANGE COLUMN `shop_id` `shop_id` DECIMAL(25,0) NOT NULL ;");
            $this->query("ALTER TABLE `progress_tracker` ADD UNIQUE INDEX `shop_id` (`shop_id`);");
        }
    }
}

<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InitialSchema extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
       $table = $this->table('users');
        $table->addColumn('name', 'string', ['limit' => 255])
            ->addColumn('email', 'string', ['limit' => 255])
            ->addColumn('email_verified_at', 'datetime')
            ->addColumn('password', 'string', ['limit' => 255])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->create();



        $productsTable = $this->table('products');
        $productsTable->addColumn('title','string',['limit' => 255])
        ->addColumn('body_html','string')
            ->addColumn('handle','string',['limit' =>255])
            ->addColumn('product_id', 'biginteger', ['limit' => 20])
            ->create();

        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `products` CHANGE COLUMN `product_id` `product_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `products` CHANGE COLUMN `body_html` `body_html` LONGTEXT NOT NULL;");
        }


        $variantsTable = $this->table('variants');
        $variantsTable->addColumn('price', 'decimal', ['limit' => 2])
            ->addColumn('product_id', 'biginteger', ['limit' => 20])
            ->addColumn('variant_id', 'biginteger', ['limit' => 20])
            ->addColumn('title','string',['limit' =>255])
            ->addColumn('sku','string',['limit' =>255])
            ->addColumn('inventory_item_id', 'biginteger', ['limit' => 20])
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `variants` CHANGE COLUMN `product_id` `product_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `variants` CHANGE COLUMN `variant_id` `variant_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `variants` CHANGE COLUMN `inventory_item_id` `inventory_item_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `variants` CHANGE COLUMN `price` `price` DECIMAL(25,2) NOT NULL;");
        }


         $optionsTable = $this->table('options');
        $optionsTable->addColumn('product_id','biginteger',['limit' => 20])
            ->addColumn('name','string',["limit"=>255])
            ->addColumn('value','string', ["limit" => 255])
            ->addColumn('option_id','biginteger',['limit' => 20])
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `options` CHANGE COLUMN `product_id` `product_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `options` CHANGE COLUMN `option_id` `option_id` DECIMAL(25,0) NOT NULL;");

        }


        $vendorsTable = $this->table('vendors');
        $vendorsTable->addColumn('name','string',['limit' => 255])
        ->addColumn('product_id','biginteger',['limit' => 20])
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `vendors` CHANGE COLUMN `product_id` `product_id` DECIMAL(25,0) NOT NULL;");
        }

        $productTypeTable = $this->table('product_types');
        $productTypeTable->addColumn('type_id','biginteger')
        ->addColumn('product_id','biginteger')
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `product_types` CHANGE COLUMN `product_id` `product_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `product_types` CHANGE COLUMN `type_id` `type_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `product_types` ADD INDEX `product_id` (`product_id`);");
        }

        $typeTable = $this->table('types');
        $typeTable->addColumn('name','string')
            ->create();


        $tagsTable = $this->table('tags');
        $tagsTable->addColumn('name','string',['limit' => 255])
        ->create();


        $productTagsTable = $this->table('product_tags');
        $productTagsTable->addColumn('tag_id','biginteger')
            ->addColumn('product_id','biginteger')
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `product_tags` CHANGE COLUMN `tag_id` `tag_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `product_tags` CHANGE COLUMN `product_id` `product_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `product_tags` ADD UNIQUE INDEX `product_tags` (`tag_id`, `product_id`);");
            $this->query("ALTER TABLE `product_tags` ADD INDEX `tag_id` (`tag_id`);");
        }


        $imagesTable = $this->table('images');
        $imagesTable->addColumn('product_id','biginteger',['limit' => 20])
        ->addColumn('src','string',['limit' => 255])
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `images` CHANGE COLUMN `product_id` `product_id` DECIMAL(25,0) NOT NULL;");
        }

        $pCollectionsTable = $this->table("product_collections");
        $pCollectionsTable->addColumn('product_id','biginteger',['limit' => 20])
            ->addColumn('collection_id','biginteger',['limit' => 20])
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `product_collections` CHANGE COLUMN `product_id` `product_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `product_collections` CHANGE COLUMN `collection_id` `collection_id` DECIMAL(25,0) NOT NULL;");

        }

        $collectionsTable = $this->table("collections");
        $collectionsTable->addColumn('title','string',["limit"=>255])
            ->addColumn('handle','string', ["limit" => 255])
            ->create();




    }
}

<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class MoreInitialSchema extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $shops = $this->table('shops');
        $shops->addColumn('name', 'string', ['limit' => 255])
            ->addColumn('email', 'string', ['limit' => 255])
            ->addColumn('domain', 'string', ['limit' => 255])
            ->addColumn('province', 'string', ['limit' => 255])
            ->addColumn('country', 'string', ['limit' => 255])
            ->addColumn('city', 'string', ['limit' => 255])
            ->addColumn('country_code', 'string', ['limit' => 255])
            ->addColumn('country_name', 'string', ['limit' => 255])
            ->addColumn('customer_email', 'string', ['limit' => 255])
            ->addColumn('shop_owner', 'string', ['limit' => 255])
            ->addColumn('shop_id', 'biginteger', ['limit' => 20])
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `shops` CHANGE COLUMN `shop_id` `shop_id` DECIMAL(25,0) NOT NULL ;");
        }

        $currencies = $this->table("currencies");
        $currencies->addColumn("name", "string", ["limit" => 255])
            ->addColumn("shop_id", "integer")
            ->create();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `currencies` CHANGE COLUMN `shop_id` `shop_id` DECIMAL(25,0) NOT NULL ;");
        }

        $products = $this->table("products");
        $products->addColumn("created_at", "datetime")
            ->addColumn("updated_at", "datetime")
            ->addColumn("shop_id", "integer")
            ->addColumn("active", "integer")
            ->update();
        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `products` CHANGE COLUMN `shop_id` `shop_id` DECIMAL(25,0) NOT NULL ;");
        }

        $images = $this->table("images");
        $images->addColumn("width", "string")
            ->addColumn("height", "string")
            ->update();

        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `product_collections` ADD UNIQUE INDEX `collection_product` (`collection_id`, `product_id`);");
            $this->query("ALTER TABLE `product_collections` ADD INDEX `product_id` (`product_id`);");
            $this->query("ALTER TABLE `product_collections` ADD INDEX `collection_id` (`collection_id`);");

            $this->query("ALTER TABLE `collections` ADD INDEX `title` (`title`);");
            $this->query("ALTER TABLE `collections` ADD UNIQUE INDEX `uniquetitle` (`title`);");

            $this->query("ALTER TABLE `currencies` ADD INDEX `shop_id` (`shop_id`);");
            $this->query("ALTER TABLE `currencies` ADD UNIQUE INDEX `shop_name` (`shop_id`, `name`);");

            $this->query("ALTER TABLE `images` ADD INDEX `product_id` (`product_id`);");
            $this->query("ALTER TABLE `images` ADD UNIQUE INDEX `product_src` (`src`, `product_id`);");


            $this->query("ALTER TABLE `options` ADD INDEX `product_id` (`product_id`);");
            $this->query("ALTER TABLE `options` ADD INDEX `option_id` (`option_id`);");
            $this->query("ALTER TABLE `options` ADD UNIQUE INDEX `product_name_value` (`product_id`, `name`, `value`);");


            $this->query("ALTER TABLE `products` ADD INDEX `product_id` (`product_id`);");
            $this->query("ALTER TABLE `products` ADD INDEX `shop_id` (`shop_id`);");
            $this->query("ALTER TABLE `products` ADD INDEX `active` (`active`);");
            $this->query("ALTER TABLE `products` ADD FULLTEXT INDEX `body_html` (`body_html`);");
            $this->query("ALTER TABLE `products` ADD FULLTEXT INDEX `handle` (`handle`);");
            $this->query("ALTER TABLE `products` CHANGE COLUMN `created_at` `created_at` DATETIME NOT NULL DEFAULT NOW() AFTER `product_id`, ADD INDEX `created_at` (`created_at`);");
            $this->query("ALTER TABLE `products` CHANGE COLUMN `updated_at` `updated_at` DATETIME NOT NULL DEFAULT NOW() AFTER `created_at`, ADD INDEX `updated_at` (`updated_at`);");
            $this->query("ALTER TABLE `products` ADD UNIQUE INDEX `product_shop_id` (`product_id`, `shop_id`);");


            $this->query("ALTER TABLE `shops` ADD FULLTEXT INDEX `name` (`name`);");
            $this->query("ALTER TABLE `shops` ADD UNIQUE INDEX `shop_id` (`shop_id`);");

            $this->query("ALTER TABLE `tags` ADD FULLTEXT INDEX `name` (`name`);");

            $this->query("ALTER TABLE `types` ADD FULLTEXT INDEX `name` (`name`);");

            $this->query("ALTER TABLE `variants` ADD INDEX `price` (`price`);");
            $this->query("ALTER TABLE `variants` ADD INDEX `product_id` (`product_id`);");
            $this->query("ALTER TABLE `variants` ADD INDEX `variant_id` (`variant_id`);");
            $this->query("ALTER TABLE `variants` ADD FULLTEXT INDEX `title` (`title`);");
            $this->query(" ALTER TABLE `variants` ADD UNIQUE INDEX `product_variant` (`variant_id`, `product_id`);");


            $this->query("ALTER TABLE `vendors` ADD FULLTEXT INDEX `name` (`name`);");
            $this->query("ALTER TABLE `vendors` ADD INDEX `product_id` (`product_id`);");
            $this->query("ALTER TABLE `vendors` ADD UNIQUE INDEX `product_name` (`name`, `product_id`);");

            $this->query("ALTER TABLE `product_types` ADD INDEX `type_id` (`type_id`);");
            $this->query("ALTER TABLE `product_tags` ADD INDEX `product_id` (`product_id`);");
        }


    }
}

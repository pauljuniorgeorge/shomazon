<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class NoTextSearching extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
            $variants = $this->table("variants");
            $variants->addColumn("title_id","integer");
            $variants->update();

            $variant_titles = $this->table("variant_titles");
           $variant_titles->addColumn("variant_title","string", ["limit"=>255]);
            $variant_titles->create();

            $vendors = $this->table("vendors");
            $vendors->addColumn("vendor_name_id","integer");
            $vendors->update();

        $vendorNames = $this->table("vendor_names");
        $vendorNames->addColumn("vendor_name","string", ["limit"=>255]);
        $vendorNames->create();

       $countryNames = $this->table("country_names");
        $countryNames->addColumn("country_name","string", ["limit" => 255 ]);
        $countryNames->create();

        $provinceNames = $this->table("province_names");
        $provinceNames->addColumn("province_name","string", ["limit" => 255 ]);
        $provinceNames->create();

        $cityNames = $this->table("city_names");
        $cityNames->addColumn("city_name","string", ["limit" => 255 ]);
        $cityNames->create();

        $shops = $this->table("shops");
        $shops->addColumn("city_id","biginteger");
        $shops->addColumn("country_id","biginteger");
        $shops->addColumn("province_id","biginteger");
        $shops->update();


        if ($this->isMigratingUp()) {
            $this->query("ALTER TABLE `variants` CHANGE COLUMN `title_id` `title_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `vendors` CHANGE COLUMN `vendor_name_id` `vendor_name_id` DECIMAL(25,0) NOT NULL;");
            $this->query("ALTER TABLE `variants` ADD INDEX `title_id` (`title_id`);");
            $this->query("ALTER TABLE `vendors` ADD INDEX `vendor_name_id` (`vendor_name_id`);");
            $this->query(" ALTER TABLE `vendor_names` ADD UNIQUE INDEX `vname` (`vendor_name`);");
            $this->query(" ALTER TABLE `variant_titles` ADD UNIQUE INDEX `vtitle` (`variant_title`);");

            $this->query(" ALTER TABLE `country_names` ADD UNIQUE INDEX `country_name` (`country_name`);");
            $this->query(" ALTER TABLE `province_names` ADD UNIQUE INDEX `province_name` (`province_name`);");
            $this->query(" ALTER TABLE `city_names` ADD UNIQUE INDEX `city_name` (`city_name`);");


        }




    }
}

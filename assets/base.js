// Functions JS
function infoToast(message, hideAfter = false){
    $.toast({
        text: message, // Text that is to be shown in the toast
        heading: 'Info', // Optional heading to be shown on the toast
        icon: 'info', // Type of toast icon
        showHideTransition: 'fade', // fade, slide or plain
        allowToastClose: true, // Boolean value true or false
        hideAfter: hideAfter, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
        stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
        position: 'bottom-left', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
        textAlign: 'left',  // Text alignment i.e. left, right or center
        loader: true,  // Whether to show loader or not. True by default
        loaderBg: '#9EC600',  // Background color of the toast loader
        beforeShow: function () {}, // will be triggered before the toast is shown
        afterShown: function () {}, // will be triggered after the toat has been shown
        beforeHide: function () {}, // will be triggered before the toast gets hidden
        afterHidden: function () {}  // will be triggered after the toast has been hidden
    });
}function errorToast(message, hideAfter = false){
    $.toast({
        text: message, // Text that is to be shown in the toast
        heading: "Error", // Optional heading to be shown on the toast
        icon: 'error', // Type of toast icon
        showHideTransition: 'fade', // fade, slide or plain
        allowToastClose: true, // Boolean value true or false
        hideAfter: hideAfter, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
        stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
        position: 'bottom-left', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
        textAlign: 'left',  // Text alignment i.e. left, right or center
        loader: true,  // Whether to show loader or not. True by default
        loaderBg: '#9EC600',  // Background color of the toast loader
        beforeShow: function () {}, // will be triggered before the toast is shown
        afterShown: function () {}, // will be triggered after the toat has been shown
        beforeHide: function () {}, // will be triggered before the toast gets hidden
        afterHidden: function () {}  // will be triggered after the toast has been hidden
    });
}
function loginRequired(){
    $.toast({
        text: "You must login to Perform this action", // Text that is to be shown in the toast
        heading: 'Login Required', // Optional heading to be shown on the toast
        icon: 'info', // Type of toast icon
        showHideTransition: 'fade', // fade, slide or plain
        allowToastClose: true, // Boolean value true or false
        hideAfter: false, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
        stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
        position: 'bottom-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
        textAlign: 'left',  // Text alignment i.e. left, right or center
        loader: true,  // Whether to show loader or not. True by default
        loaderBg: '#9EC600',  // Background color of the toast loader
        beforeShow: function () {}, // will be triggered before the toast is shown
        afterShown: function () {}, // will be triggered after the toat has been shown
        beforeHide: function () {}, // will be triggered before the toast gets hidden
        afterHidden: function () {}  // will be triggered after the toast has been hidden
    });
}

function reloadPage(){
    window.location = window.location.origin;
}

function removeProducts(){
    infoToast("Removing Products", 3000);
    $.post( "index.php", {
            action: "removeproducts",
            ajax: "true"
        },
        function( data ) {

        if(data.status == true){
            infoToast("Removed All Products");
        }
        else{
            errorToast("There was a problem removing products. Contact the webmaster");
        }
            setTimeout(reloadPage,2000);
        }, "json");
}

function importProducts(){

    window.progressUpdate = setTimeout(updateProgress,1000);
    $("#importMyProducts").html("Import/Track Store 0%");
    $.post( "index.php", {
            action: "import",
            ajax: "true"
        },
        function( data ) {
        console.log(data);

        }, "json");
}
function logOut(){

    $.post( "index.php", {
            action: "logout",
            ajax: "true"
        },
        function( data ) {
            window.location.reload();
        }, "json");
}

function updateProgress(){

    clearTimeout(window.progressUpdate);

    $.post( "index.php", {
            action: "progressUpdate",
            ajax: "true"
        },
        function( data ) {
        console.log("Hit");
        console.log(data);
        console.log(data.message);
        completion = parseInt(100 * data.message);
        $("#importMyProducts").html("Import/Track Store "+completion+"%");
        if(data.message < 1){
            updateProgress();
        }
        else{
            infoToast("Imported Products Successfully");
            $("#importMyProducts").html("Import/Track Store Complete");
            //setTimeout(reloadPage,2000);
        }

        }, "json");
}



function updateMultipleParam(param,value){
// Construct URLSearchParams object instance from current URL querystring.
    queryParams = new URLSearchParams(window.location.search);
    queryParams.delete(param);

// Set new or modify existing parameter value.
     for(i=0;i<value.length;i++){
            queryParams.append(param, value[i]);
        }
    // Replace current querystring with the new one.
    history.replaceState(null, null, "?"+queryParams.toString());
}
function updateSingleParam(param,value){
// Construct URLSearchParams object instance from current URL querystring.
    queryParams = new URLSearchParams(window.location.search);
    queryParams.set(param, value);

    if(param=="query" && value.length < 1){
        return;
    }

    // Replace current querystring with the new one.
    history.replaceState(null, null, "?"+queryParams.toString());
}

function query(){
    clearTimeout(window.searchStall);

    $("select[name='types[]']").prop("disabled", true);
    $("select[name='collections[]']").prop("disabled", true);
    $("select[name='variants[]']").prop("disabled", true);
    $("select[name='vendors[]']").prop("disabled", true);
    $("select[name='tags[]']").prop("disabled", true);
    $("select[name='country[]']").prop("disabled", true);
    $("select[name='region[]']").prop("disabled", true);
    $("select[name='city[]']").prop("disabled", true);
    $("input[name='query']").prop("disabled", true);

    updateMultipleParam("types[]",$("select[name='types[]']").val());
    updateMultipleParam("collections[]",$("select[name='collections[]']").val());
    updateMultipleParam("variants[]",$("select[name='variants[]']").val());
    updateMultipleParam("vendors[]",$("select[name='vendors[]']").val());
    updateMultipleParam("tags[]",$("select[name='tags[]']").val());
    updateMultipleParam("country[]",$("select[name='country[]']").val());
    updateMultipleParam("region[]",$("select[name='region[]']").val());
    updateMultipleParam("city[]",$("select[name='city[]']").val());
    updateSingleParam("query",$("input[name='query']").val());


    $("#content").fadeTo("slow",0.5);
    infoToast("Retreiving More Products",1200);

    $.post( "index.php", {
            action: "query",
            ajax: "true",
            types: $("select[name='types[]']").val(),
            collections: $("select[name='collections[]']").val(),
            variants: $("select[name='variants[]']").val(),
            vendors:$("select[name='vendors[]']").val(),
            tags: $("select[name='tags[]']").val(),
            country: $("select[name='country[]']").val(),
            region: $("select[name='region[]']").val(),
            city: $("select[name='city[]']").val(),
            query: $("input[name='query']").val(),
            limit: 50,
            offset: window.offset
        },
        function( data ) {
            payload = JSON.parse(data.payload);

            if(window.offset == 0){

                $("#content").html(payload.html);
            }
            else{
                $("#content").append(payload.html);
            }

            $("#content").fadeTo("slow",1);


            applicableFiltersUpdate(payload.basesql,payload.query);
            window.offset = window.offset + window.limit;
            }, "json");


}// function query()

function applicableFiltersUpdate(basesql,query){
   console.log("aFU");
   console.log(query);

    $.post( "index.php", {
            action: "update_filters",
            ajax: "true",
            query: query,
            basesql: basesql
        },

        function( data ) {
            var options = "";

            console.log(data);
        payload = JSON.parse(data.payload);

        //Types
        typesSetTo = $("select[name='types[]']").val();
        for(i=0; i < payload.types.length; i++){
             if(typesSetTo.includes(payload.types[i].type_id)){
                 options += "<option selected value='"+payload.types[i].type_id+"'>"+payload.types[i].name+" ("+payload.types[i].type_matches +")</option>";
             }
             else{
                 options += "<option value='"+payload.types[i].type_id+"'>"+payload.types[i].name+" ("+payload.types[i].type_matches+")</option>";
             }
        }
        $("select[name='types[]']").html(options);
        //End Types

            //Collections
            options = "";
            collectionsSetTo = $("select[name='collections[]']").val();
            for(i=0; i < payload.collections.length; i++){
                if(collectionsSetTo.includes(payload.collections[i].collection_id)){
                    options += "<option selected value='"+payload.collections[i].collection_id+"'>"+payload.collections[i].title+" ("+payload.collections[i].collection_matches+")</option>";
                }
                else{
                    options += "<option value='"+payload.collections[i].collection_id+"'>"+payload.collections[i].title+" ("+payload.collections[i].collection_matches+")</option>";
                }
            }
            $("select[name='collections[]']").html(options);
            //End Collections

            //Vendors
            options = "";
            vendorsSetTo = $("select[name='vendors[]']").val();
            for(i=0; i < payload.vendors.length; i++){
                if(vendorsSetTo.includes(payload.vendors[i].vendor_name_id)){
                    options += "<option selected value='"+payload.vendors[i].vendor_name_id+"'>"+payload.vendors[i].name+" ("+payload.vendors[i].vendor_matches+")</option>";
                }
                else{
                    options += "<option value='"+payload.vendors[i].vendor_name_id+"'>"+payload.vendors[i].name+" ("+payload.vendors[i].vendor_matches+")</option>";
                }
            }
            $("select[name='vendors[]']").html(options);
            //End Vendors

            //Variants
            options = "";
            variantsSetTo = $("select[name='variants[]']").val();
            for(i=0; i < payload.variants.length; i++){
                if(variantsSetTo.includes(payload.variants[i].title_id)){
                    options += "<option selected value='"+payload.variants[i].title_id+"'>"+payload.variants[i].title+" ("+payload.variants[i].title_matches+")</option>";
                }
                else{
                    options += "<option value='"+payload.variants[i].title_id+"'>"+payload.variants[i].title+" ("+payload.variants[i].title_matches+")</option>";
                }
            }
            $("select[name='variants[]']").html(options);
            //End Variants

            //tags
            options = "";
            tagsSetTo = $("select[name='tags[]']").val();
            for(i=0; i < payload.tags.length; i++){
                if(tagsSetTo.includes(payload.tags[i].tag_id)){
                    options += "<option selected value='"+payload.tags[i].tag_id+"'>"+payload.tags[i].name+" ("+payload.tags[i].tag_matches+")</option>";
                }
                else{
                    options += "<option value='"+payload.tags[i].tag_id+"'>"+payload.tags[i].name+" ("+payload.tags[i].tag_matches+")</option>";
                }
            }
            $("select[name='tags[]']").html(options);
            //tags

            //country
            options = "";
            countrySetTo = $("select[name='country[]']").val();
            for(i=0; i < payload.countries.length; i++){
                if(countrySetTo.includes(payload.countries[i].country_id)){
                    options += "<option selected value='"+payload.countries[i].country_id+"'>"+payload.countries[i].country_name+" ("+payload.countries[i].country_matches+")</option>";
                }
                else{
                    options += "<option value='"+payload.countries[i].country_id+"'>"+payload.countries[i].country_name+" ("+payload.countries[i].country_matches+")</option>";
                }
            }
            $("select[name='country[]']").html(options);
            //country

            //region
            options = "";
            regionSetTo = $("select[name='region[]']").val();
            for(i=0; i < payload.regions.length; i++){
                if(regionSetTo.includes(payload.regions[i].province_id)){
                    options += "<option selected value='"+payload.regions[i].province_id+"'>"+payload.regions[i].province+" ("+payload.regions[i].region_matches+")</option>";
                }
                else{
                    options += "<option value='"+payload.regions[i].province_id+"'>"+payload.regions[i].province+" ("+payload.regions[i].region_matches+")</option>";
                }
            }
            $("select[name='region[]']").html(options);
            //end region

            //city
            options = "";
            citySetTo = $("select[name='city[]']").val();
            for(i=0; i < payload.cities.length; i++){
                if(citySetTo.includes(payload.cities[i].city_id)){
                    options += "<option selected value='"+payload.cities[i].city_id+"'>"+payload.cities[i].city+" ("+payload.cities[i].city_matches+")</option>";
                }
                else{
                    options += "<option value='"+payload.cities[i].city_id+"'>"+payload.cities[i].city+" ("+payload.cities[i].city_matches+")</option>";
                }
            }
            $("select[name='city[]']").html(options);
            //end city

            window.dontUpdate = true;
            $("select[name='types[]']").trigger("change");
            $("select[name='collections[]']").trigger("change");
            $("select[name='variants[]']").trigger("change");
            $("select[name='vendors[]']").trigger("change");
            $("select[name='tags[]']").trigger("change");
            $("select[name='country[]']").trigger("change");
            $("select[name='region[]']").trigger("change");
            $("select[name='city[]']").trigger("change");
            window.dontUpdate = false;

            $("select[name='types[]']").prop("disabled", false);
            $("select[name='collections[]']").prop("disabled", false);
            $("select[name='variants[]']").prop("disabled", false);
            $("select[name='vendors[]']").prop("disabled", false);
            $("select[name='tags[]']").prop("disabled", false);
            $("select[name='country[]']").prop("disabled", false);
            $("select[name='region[]']").prop("disabled", false);
            $("select[name='city[]']").prop("disabled", false);
            $("input[name='query']").prop("disabled", false);
            //$("#content").html(data.payload);
            console.log("UpdatedApplicableFilters");
            console.log(data);
            }, "json");
}

$( document ).ready(function() {

    window.dontUpdate = false;
    window.offset = 0;
    window.limit = 50;

    $("select").select2();

    $(".btn.import").click(function(){
        $("#importform").submit();
    });

    $("select[name='types[]']," +
        "select[name='collections[]']," +
        "select[name='variants[]']," +
        "select[name='vendors[]']," +
        "select[name='tags[]']," +
        "select[name='country[]']," +
        "select[name='region[]']," +
        "select[name='city[]']").change(function(){
   if(window.dontUpdate == false){
       window.offset = 0;
       query();
   }
    });

    $("input[name='query']").keyup(function(){
        clearTimeout(window.searchStall);
        window.offset = 0;
        window.searchStall = setTimeout(query,500);
    });

    $(window).scroll(function() {
        console.log("Scroll")
        console.log(window.innerHeight);
        console.log(window.scrollY);
        console.log(document.body.offsetHeight);

        if((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            window.offset = window.offset + window.limit;
            query();
        }
    });

    if($("#queryString").val() == "1"){
        query();
    }
    /*$("#filters").resizable({
        resize: function(event, ui) {
            ui.size.height = ui.originalSize.height;
        }
    });*/
    $(".select2-container").click(function(){
        console.log("container click");
        $(".select2-dropdown").width("291px");
    });



});
//$( document ).ready(function() {


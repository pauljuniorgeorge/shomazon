<?php
error_log('The Eagle has Landed: ...'); //check error.log to see the result

require 'vendor/autoload.php';
require 'config.php';
require 'functions.php';
use LukeTowers\ShopifyPHP\Shopify;
$db = new MysqliDb ('localhost', DB_USER, DB_PASSWORD, DB);


function verify_webhook($data, $hmac_header)
{
    $calculated_hmac = base64_encode(hash_hmac('sha256', $data, API_SECRET, true));
    return hash_equals($hmac_header, $calculated_hmac);
}



$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$data = file_get_contents('php://input');
$verified = verify_webhook($data, $hmac_header);
error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result

$headers = getallheaders();
$topic = $headers['X-Shopify-Topic'];
$storeName = $headers['X-Shopify-Shop-Domain'];

$data = json_decode($data,true);



if ($topic == 'products/create') {
error_log($data["id"]);
error_log("PRODUCTS CREATE");
//    error_log(print_r($data,true));
error_log($storeName);
addProduct($data,$storeName);
}

if($topic == "products/delete"){

error_log("DELETE ID");
error_log($data["id"]);
deleteProduct($data["id"]);

}

if($topic == "shop/update"){
error_log("before update");
updateShop($data);
error_log("afterupdate");
}

if($topic == "app/uninstalled"){
error_log("Before app uninstalled");
uninstallApp($data,$storeName);
error_log("After app uninstalled");
}

if($topic == "products/update"){
error_log("Before product update");
updateProduct($data,$storeName);
error_log($db->getLastError());
error_log("After product update");
}




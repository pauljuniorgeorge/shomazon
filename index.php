<?php
require 'vendor/autoload.php';
require 'config.php';
require 'functions.php';
use LukeTowers\ShopifyPHP\Shopify;
$db = new MysqliDb ('localhost', DB_USER, DB_PASSWORD, DB);




function check_authorization_attempt()
{
$data = $_GET;

$api = new Shopify($data['shop'], [
'api_key' => API_KEY,
'secret'  => API_SECRET
]);

$storedAttempt = null;
$attempts = array_reverse(json_decode(file_get_contents('authattempts.json')));
foreach ($attempts as $attempt) {
if ($attempt->shop === $data['shop']) {
$storedAttempt = $attempt;
break;
}
}

return $api->authorizeApplication($storedAttempt->nonce, $data);
}

if(isset($_GET["code"])){
    $response = check_authorization_attempt();
    if ($response) {
        setcookie("access_token", $response->access_token, strtotime("+1 year"));
        setcookie("shop", $_GET["shop"], strtotime("+1 year"));
        $access_token = $response->access_token;

        $config = array(
            'ShopUrl' => $_GET["shop"],
            'AccessToken' => $access_token,
        );
        $shopify = PHPShopify\ShopifySDK::config($config);
        $shop = $shopify->Shop()->get();

        $db->where("domain",$_GET["shop"]);
        $shop = $db->get("shops");
        if(empty($shop)){
            addShop($shop,$access_token);
        }
    }
    header("Location: ".REDIRECT_URI);
}

if(ajax_request()){

    switch($_POST["action"]){

        case 'import':

            if(is_user()) {
                $access_token = isset($_COOKIE["access_token"]) ? $_COOKIE["access_token"] : $access_token;
                $api = new Shopify($_COOKIE["shop"], $access_token);
                $config = array(
                    'ShopUrl' => $_COOKIE["shop"],
                    'AccessToken' => $access_token,
                );
                $shopify = PHPShopify\ShopifySDK::config($config);
                $productResource = $shopify->Product();
                $productCount = $shopify->Product->count();
                $collectResource = $shopify->Collect();
                $collectsCount = $collectResource->count();
                $shop = $shopify->Shop()->get();

                $webhooks = $shopify->Webhook->get();
                $existingWh = [];
                foreach($webhooks as $wh){
                    $existingWh[] = $wh["topic"];
                }

                $toCreate = [];
                // Create webhooks
                if(!in_array("products/create",$existingWh)){
                    $toCreate[] = array('topic' => 'products/create', 'address' => WEBHOOK_URI."webhooks.php", 'format' => 'json');
                }
                if(!in_array("products/update",$existingWh)) {
                    $toCreate[] = array('topic' => 'products/update', 'address' => WEBHOOK_URI."webhooks.php", 'format' => 'json');
                }
                if(!in_array("products/delete",$existingWh)) {
                    $toCreate[] = array('topic' => 'products/delete', 'address' => WEBHOOK_URI."webhooks.php", 'format' => 'json');
                }

                if(!in_array("shop/update",$existingWh)) {
                    $toCreate[] = array('topic' => 'shop/update', 'address' => WEBHOOK_URI."webhooks.php", 'format' => 'json');
                }
                if(!in_array("app/uninstalled",$existingWh)) {
                    $toCreate[] = array('topic' => 'app/uninstalled', 'address' => WEBHOOK_URI."webhooks.php", 'format' => 'json');
                }


                try {
                    foreach($toCreate as $next){
                        $shopify->Webhook->post($next);
                    }
                }
                catch(Exception $e){
                    echo $e->getMessage();
                    exit();
                }

                $webhooks = $shopify->Webhook->get();

                // COUNT SMART COLLECTS
                $smart = $shopify->SmartCollection();
                $smarts = $smart->get();
                $count = 0;
                $saveSmartCollections = [];

                while ($smarts) {
                    foreach ($smarts as $curSmart) {
                        $resource = $shopify->Collection($curSmart["id"])->Product();
                        $results = $resource->get();

                        $count = $count + count($results);

                        $saveSmartCollections[$curSmart["id"]]["smartCollection"] = $curSmart;
                        $saveSmartCollections[$curSmart["id"]]["products"] = [];
                        while ($results) {

                            foreach($results as $result){
                                $saveSmartCollections[$curSmart["id"]]["products"][] = $result;
                            }

                            if (!empty($resource->getNextPageParams())) {
                                $results = $resource->get($resource->getNextPageParams());
                            } else {
                                $results = false;
                            }
                        }
                    }
                    if (!empty($smart->getNextPageParams())) {
                        $smarts = $smart->get($smart->getNextPageParams());
                    } else {
                        $smarts = false;
                    }
                }//Foreach smarts

                // COUNT SMART COLLECTS END
                $count = $count + $productCount + $collectsCount;

                $db->replace("progress_tracker",[
                        "shop_id" => $shop["id"],
                        "total" => $count,
                        "last_progress_count" => 0
                ]);





                $products = $productResource->get(['limit' => 250]);

                while ($products) {
                    foreach ($products as $product) {
                     try{
                         addProduct($product,$shop["domain"]);
                     }//end try
                     catch(Exception $e){
                         response("false", $e->getMessage());
                     }//end catch
                    }// end foreach

                    if (!empty($productResource->getNextPageParams())) {
                        try{
                            $products = $productResource->get($productResource->getNextPageParams());
                        }
                        catch(Exception $e)
                        {
                            response("false",$e->getMessage());
                        }
                    } else {
                        $products = false;
                    }
                }


                $collects = $collectResource->get();
                $collections = [];
                while ($collects) {

                    foreach ($collects as $collect) {
                        try{
                            addCollect($collect);
                        }
                        catch(Exception $e){
                            response(false,$e->getMessage());
                        }
                    }

                    if (!empty($collectResource->getNextPageParams())) {
                        $collects = $collectResource->get($collectResource->getNextPageParams());
                    } else {
                        $collects = false;
                    }
                }

                foreach ($saveSmartCollections as $key => $smartCollection) {
                    // Lookup Collection By Name
                    $db->where("title", strtolower($smartCollection["smartCollection"]["title"]));
                    $collectionDB = $db->getOne("collections");
                    if (empty($collectionDB)) {
                        $id = $db->insert("collections", array(
                            "title" => strtolower($smartCollection["smartCollection"]["title"]),
                            "handle" => strtolower($smartCollection["smartCollection"]["handle"])
                        ));
                    } else {
                        $id = $collectionDB["id"];
                    }

                    foreach ($smartCollection["products"] as $product) {
                        $db->insert("product_collections",
                            array(
                                "product_id" => $product["id"],
                                "collection_id" => $id
                            ));
                    }
                }
               echo response(true,"Completed Import");
            }
            break;
        case 'progressUpdate':

            $db->where("domain",$_COOKIE["shop"]);
            $curShop = $db->get("shops");

            while(empty($curShop)){
                sleep(1);
                $db->where("domain",$_COOKIE["shop"]);
                $curShop = $db->get("shops");
            }

            $db->where ('shop_id', $curShop[0]["shop_id"]);
            $productCount = $db->getValue ('products', "count(*)");

            $db->join("product_collections pc", "pc.product_id=p.product_id", "INNER");
            $collectionsCount = count($db->get("products p", null, "*"));


            $db->where ('shop_id', $curShop[0]["shop_id"]);
            $progressCount = $db->get('progress_tracker');
            while($progressCount[0]['total'] < 1){
                sleep(1);
                $db->where ('shop_id', $curShop[0]["shop_id"]);
                $progressCount = $db->get('progress_tracker');

            }
            $completion =(intval($productCount) + intval($collectionsCount))/  intval($progressCount[0]['total']);


            response(true,$completion);
            break;

        case "logout":
            setcookie("access_token", "", time()-3600);
            setcookie("shop", "", time()-3600);
            response(true,"Logged Out");
            break;
        case "query":

            $select = "SELECT variants.variant_id,variants.product_id, variants.price,variants.title AS variant_title ,products.title AS product_title, products.body_html AS html, products.created_at as product_creation_date, CONCAT(shops.domain,'/products/',products.handle) as linked_url, shops.domain as domain, collections.title AS collection_title, product_tags.tag_id AS product_tag_id, images.src as image_src, images.width as image_width, images.height as image_height ";
            $from = "FROM variants 
INNER JOIN products ON products.product_id = variants.product_id
INNER JOIN shops ON products.shop_id = shops.shop_id 
INNER JOIN images on products.product_id = images.product_id
";
            $from .= "LEFT JOIN product_collections ON products.product_id = product_collections.product_id ";
            $from .= "LEFT JOIN product_tags ON products.product_id = product_tags.product_id ";
            $from .= "LEFT JOIN product_types ON products.product_id = product_types.product_id ";
            $from .= "LEFT JOIN vendors ON vendors.product_id = products.product_id ";
            $from .= "LEFT JOIN collections ON product_collections.collection_id = collections.id ";

            $from .= "LEFT JOIN tags ON product_tags.tag_id = tags.id ";
            $from .= "LEFT JOIN types ON product_types.type_id = types.id ";

            $orderBy = "ORDER BY ";

            if(isset($_POST["types"]) || isset($_POST["collections"]) || isset($_POST["variants"]) || isset($_POST["tags"]) || isset($_POST["vendors"]) || isset($_POST["country"]) || isset($_POST["region"]) || isset($_POST["city"])){
                $where = "WHERE ";
            }
            if(isset($_POST["types"])){
                $where .= ($where == "WHERE ")? "" : "AND ";
                $where .= "product_types.type_id IN(".implode(",", $_POST["types"]).")";
            }
            if(isset($_POST["collections"])){
                $where .= ($where == "WHERE ")? "" : "AND ";
                $where .= "product_collections.collection_id IN(".implode(",",$_POST["collections"]).") ";
            }
            if(isset($_POST["variants"])){
                $where .= ($where == "WHERE ")? "" : "AND ";
                $where .= 'variants.title_id IN('.implode(', ',$_POST["variants"]).') ';
            }
            if(isset($_POST["tags"])){
                $where .= ($where == "WHERE ")? "" : "AND ";
                $where .= " product_tags.tag_id IN(".implode(",",$_POST["tags"]).") ";
            }

            if(isset($_POST["vendors"])){
                $where .= ($where == "WHERE ")? "" : "AND ";
                $where .= 'vendors.vendor_name_id IN('.implode(', ',$_POST["vendors"]).') ';
            }
            if(isset($_POST["country"])){
                $where .= ($where == "WHERE ")? "" : "AND ";
                $where .= 'shops.country_name IN('.implode(', ',$_POST["country"]).') ';
            }
            if(isset($_POST["region"])){
                $where .= ($where == "WHERE ")? "" : "AND ";
                $where .= 'shops.province IN('.implode(', ',$_POST["region"]).') ';
            }
            if(isset($_POST["city"])){
                $where .= ($where == "WHERE ")? "" : "AND ";
                $where .= 'shops.city IN('.implode(', ',$_POST["city"]).') ';
            }

            if(isset($_POST["query"]) AND strlen($_POST["query"]) > 2){
                $select .= " , MATCH (products.body_html, products.handle, products.title) AGAINST ('".$_POST['query']."') AS relevance ,  MATCH(collections.title) AGAINST ('".$_POST['query']."') as collectiontitlerelevance, MATCH(variants.title) AGAINST ('".$_POST['query']."') as variantrelevance, MATCH(vendors.name) AGAINST ('".$_POST['query']."') as vendornamerelevance, MATCH(types.name) AGAINST ('".$_POST['query']."') as typenamerelevance ";
                $where .= "AND MATCH (products.body_html, products.handle, products.title) AGAINST ('".$_POST['query']."') OR MATCH(collections.title) AGAINST ('".$_POST['query']."') OR MATCH(variants.title) AGAINST ('".$_POST['query']."') OR MATCH (vendors.name) AGAINST ('".$_POST['query']."') OR MATCH(types.name) AGAINST ('".$_POST['query']."') ";
                $orderBy .= "relevance DESC, collectiontitlerelevance DESC, variantrelevance DESC, vendornamerelevance DESC, typenamerelevance DESC, ";
            }


            $groupBy = " GROUP BY variants.product_id ";

            if(isset($_POST["query"]) AND strlen($_POST["query"]) > 2){
               $groupBy .= "having (relevance > 0 OR collectiontitlerelevance > 0 or variantrelevance > 0 OR vendornamerelevance > 0 OR typenamerelevance > 0) ";
            }

            $orderBy .= "products.created_at DESC ";
            $limit = " LIMIT ".$_POST["limit"];
            $offset = " OFFSET ".$_POST["offset"];

            if(empty($where)){
                $where = "";
            }

            $q = $from.$where.$groupBy.$orderBy; // base query used only need to select variant ids happens in applicable updates
            $finalQuery = $select.$from.$where.$groupBy.$orderBy.$limit.$offset;

            $products = $db->query($finalQuery);

            $loader = new \Twig\Loader\FilesystemLoader('assets/templates');
            $twig = new \Twig\Environment($loader);
            $html = $twig->render('results.twig.html', ['products' => $products]);

            $payload = ["html" => $html, "search_results" => json_encode($products), "basesql" => $q, "query" => $_POST['query']];

            response(true,"Query",$payload);
            break;

        case "update_filters":

            $baseQuery = $_POST["basesql"];

            if(strpos($baseQuery,"MATCH") !== FALSE){
                $select = "SELECT MATCH (products.body_html, products.handle, products.title) AGAINST ('".$_POST['query']."') AS relevance ,  MATCH(collections.title) AGAINST ('".$_POST['query']."') as collectiontitlerelevance, MATCH(variants.title) AGAINST ('".$_POST['query']."') as variantrelevance, MATCH(vendors.name) AGAINST ('".$_POST['query']."') as vendornamerelevance, MATCH(types.name) AGAINST ('".$_POST['query']."') as typenamerelevance, variants.variant_id ";
                $q = $select . $baseQuery;
                $fullResultSet = $db->query($q);
                $variantIds = [];
                $totalResults = count($fullResultSet);
                $count = 0;
                foreach($fullResultSet as $result){
                    $count++;
                    $subQuery .= $result["variant_id"];
                    if($count < $totalResults)
                    {
                        $subQuery.=",";
                    }
                }
            }
            else{
                $select = "SELECT variants.variant_id ";
                $subQuery = $select.$baseQuery;
            }

            // Applicable Types
            $c = "SELECT product_types.type_id, count(types.id) as type_matches,types.name, count(types.id) as type_matches FROM types,product_types,variants WHERE variants.variant_id IN($subQuery) AND product_types.type_id = types.id AND variants.product_id = product_types.product_id GROUP BY types.id ORDER BY type_matches DESC";
            $types = $db->rawQuery($c);
            $payload["types"] = $types;

            // Applicable Tags
            $c = "SELECT *, count(tags.id) as tag_matches FROM tags,product_tags,variants WHERE variants.variant_id IN($subQuery) AND product_tags.tag_id = tags.id AND variants.product_id = product_tags.product_id GROUP BY tags.id ORDER BY tag_matches DESC";
            $tags = $db->rawQuery($c);
            $payload["tags"] = $tags;

            // Applicable Collections
            $c = "SELECT *, count(collections.id) as collection_matches FROM collections,product_collections,variants WHERE variants.variant_id IN($subQuery) AND product_collections.collection_id = collections.id AND variants.product_id = product_collections.product_id GROUP BY collections.id ORDER BY collection_matches DESC";
            $collections = $db->rawQuery($c);
            $payload["collections"] = $collections;

            // Applicable Vendors
            $c = "SELECT *, count(vendors.name) as vendor_matches FROM vendors,variants WHERE variants.variant_id IN($subQuery) AND variants.product_id = vendors.product_id GROUP BY vendors.name ORDER BY vendor_matches DESC";
            $vendors = $db->rawQuery($c);
            $payload["vendors"] = $vendors;

            // Applicable Variants
            $c = "SELECT *, count(variants.title) as title_matches FROM variants WHERE variants.variant_id IN($subQuery) GROUP BY variants.title ORDER BY title_matches DESC";
            $variants = $db->rawQuery($c);
            $payload["variants"] = $variants;



            // Applicable Countries
             $c = "SELECT variants.product_id FROM variants WHERE variants.variant_id IN($subQuery) GROUP BY variants.product_id";
            $c = "SELECT shops.country_name,shops.country_id, count(shops.country_name) as country_matches  FROM products,shops WHERE products.product_id IN($c) AND products.shop_id = shops.shop_id GROUP BY shops.country_name ORDER BY country_matches DESC";
            $countries = $db->rawQuery($c);
            $payload["countries"] = $countries;

            // Applicable Regions
             $c = "SELECT variants.product_id FROM variants WHERE variants.variant_id IN($subQuery) GROUP BY variants.product_id";
            $c = "SELECT shops.province,shops.province_id, count(shops.province) as region_matches  FROM products,shops WHERE products.product_id IN($c) AND products.shop_id = shops.shop_id GROUP BY shops.province ORDER BY region_matches DESC";
            $regions = $db->rawQuery($c);
            $payload["regions"] = $regions;

            // Applicable Cities
            $c = "SELECT variants.product_id FROM variants WHERE variants.variant_id IN($subQuery) GROUP BY variants.product_id";
            $c = "SELECT shops.city,shops.city_id, count(shops.city) as city_matches  FROM products,shops WHERE products.product_id IN($c) AND products.shop_id = shops.shop_id GROUP BY shops.city ORDER BY city_matches DESC";
            $cities = $db->rawQuery($c);
            $payload["cities"] = $cities;

            response(true,"Applicable Filters",$payload);
            break;

        case "removeproducts":

            $config = array(
                'ShopUrl' => $_COOKIE["shop"],
                'AccessToken' => $_COOKIE["access_token"],
            );

            //Remove Webhooks
            $shopify = PHPShopify\ShopifySDK::config($config);
            $webhooks = $shopify->Webhook->get();

            foreach($webhooks as $webhook){
                $shopify->Webhook($webhook['id'])->delete();
            }

            $webhooks = $shopify->Webhook->get();

		error_log("WebHooks After Remove");            
		error_log(print_r($webhooks,true));
	    $db->where("domain",$_COOKIE["shop"]);
            $curShop = $db->get("shops");

            $ids = $db->subQuery();
            $ids->where ("shop_id", $curShop[0]['shop_id'], "=");
            $ids->get ("products", null, "product_id");

            $db->where ("product_id", $ids, 'in');
            $res = $db->delete("product_collections");
            $res = $db->delete("product_tags");
            $res = $db->delete("product_types");
            $res = $db->delete("images");
            $res = $db->delete("variants");
            $res = $db->delete("vendors");
            $db->where ("shop_id", $curShop[0]['shop_id'], '=');
            $db->delete("products");
            response(true,"Removed Products");
            break;
            default:
                response(false,"No Action provided");
    }

}


if(isset($_COOKIE["shop"])){
    $db->where("domain",$_COOKIE["shop"]);
    $shop = $db->get("shops");
}

?>
<html>
<head>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/base.css">
    <link rel="stylesheet" href="assets/jquery.toast.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" integrity="sha512-urpIFwfLI9ZDL81s6eJjgBF7LpG+ROXjp1oNwTj4gSlCw00KiV1rWBrfszV3uf5r+v621fsAwqvy1wRJeeWT/A==" crossorigin="anonymous" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script type="text/javascript" src="assets/jquery.toast.min.js"></script>
    <script type="text/javascript" src="assets/base.js"></script>
    <script src="https://js.stripe.com/v3"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>


</head>
<body>
<?php if (isset($_GET["r"])){
   ?>
<script>

    var r = "<?php echo $_GET["r"]; ?>";

    if(r == "success"){
        infoToast("Your payment was completed successfully. Your products will be imported automatically",false);
        importProducts();
    }

    if (r == "cancelled"){
        errorToast("Your payment attempt was cancelled and did not complete. Please try again",false);
    }

</script>
<?php } ?>

<nav class="navbar fixed-top navbar-expand-sm navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><span class="logo">Shomazon</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <input class="form-control dark form-control-dark w-100" type="text" name="query" placeholder="Search" aria-label="Search">

        <?php if(!is_imported($_COOKIE["shop"]) && is_paid($_COOKIE["shop"])){ ?>
            <button type="button" id="importMyProducts" class="btn btn-dark nav-item" onclick="<?php echo is_user() ? 'importProducts();"':'loginRequired();' ?>" >Add My Store</button>
        <?php }else if(!is_imported($_COOKIE["shop"]) && !is_paid($_COOKIE["shop"])){ ?>
            <button type="button" role="link" title="Payment Required to Import / Track your Store" id="checkout-button-price_1HM3GvJbKGTqY0DRoGtnO440" style="width:200px;color:#09bf1f;padding:5px;border:0;border-radius:4px;font-size:1em" class="btn btn-dark nav-item">Add My Store</button>
        <?php } else if (is_imported($_COOKIE["shop"]) && !is_paid($_COOKIE["shop"])){ ?>
            <button class="btn btn-dark nav-item" title="Payment Required : Your products are not currently active on the site" type="button" role="link" id="checkout-button-price_1HM3GvJbKGTqY0DRoGtnO440" style="width:200px;color:#09bf1f;padding:5px;border:0;border-radius:4px;font-size:1em">Payment Required</button>
        <?php } else{?>
            <button type="button" class="btn btn-dark nav-item" onclick="<?php echo is_user() ? 'removeProducts();"':'loginRequired();' ?>">Remove My Store</button>
        <?php }?>


        <?php if(!is_user()){ ?>
            <button type="button" class="btn btn-dark nav-item" data-toggle="modal" data-target="#importModal">Login</button>
        <?php } else { ?>
            <button type="button" class="btn btn-dark nav-item" onclick="logOut();">Logout</button>
        <?php }?>
    </div>
</nav>


<script>
    (function() {
        var stripe = Stripe('pk_test_51HM3BMJbKGTqY0DRwCeKvSxVXHlPuJzY7F5Aoi5ON9QcjT97EDOJFYpFdqCXh3Lf2a6Jbp0G75EBTObyli3mWZ7H00xNy1d7p6');

        var checkoutButton = document.getElementById('checkout-button-price_1HM3GvJbKGTqY0DRoGtnO440');
        checkoutButton.addEventListener('click', function () {
            // When the customer clicks on the button, redirect
            // them to Checkout.
            output = stripe.redirectToCheckout({
                lineItems: [{price: 'price_1HM3GvJbKGTqY0DRoGtnO440', quantity: 1}],
                mode: 'subscription',
                clientReferenceId: '<?php echo $shop[0]['id']?>',
                // Do not rely on the redirect to the successUrl for fulfilling
                // purchases, customers may not always reach the success_url after
                // a successful payment.
                // Instead use one of the strategies described in
                // https://stripe.com/docs/payments/checkout/fulfillment
                successUrl: '<?php echo REDIRECT_URI; ?>?r=success',
                cancelUrl: '<?php echo REDIRECT_URI; ?>?r=cancelled',
            })
                .then(function (result) {
                    if (result.error) {
                        // If `redirectToCheckout` fails due to a browser or network
                        // error, display the localized error message to your customer.
                        var displayError = document.getElementById('error-message');
                        displayError.textContent = result.error.message;
                    }
                });
        });
    })();
</script>


<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="#">Product Types</a>
                <select name="types[]" class="filter_select" placeholder="Select Types" multiple>
                    <?php
                    $types = $db->get('types',null,"id,name");
                    foreach($types as $type){
                        echo "<option ".((in_array($type['id'],$_GET['types']))? 'selected' : '')." value='".$type['id']."'>".$type['name']."</option>";
                    }
                    ?>
                </select>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Product Vendors</a>
                <select name="vendors[]" class="filter_select" placeholder="Select Vendors" multiple>
                    <?php
                    $vendors = $db->get('vendor_names',null,"id,vendor_name");
                    foreach($vendors as $vendor){
                        echo "<option ".((in_array($vendor['id'],$_GET['vendors']))? 'selected' : '')." value='".$vendor['id']."'>".$vendor['vendor_name']."</option>";
                    }
                    ?>
                </select>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                   Product Categories
                </a>
                <select name="collections[]" class="filter_select" placeholder="Select collections" multiple>
                    <?php
                    $collections = $db->get('collections',null,"id,title");
                    foreach($collections as $collection){
                        echo "<option ".((in_array($collection['id'],$_GET['collections']))? 'selected' : '')." value='".$collection['id']."'>".$collection['title']."</option>";
                    }
                    ?>
                </select>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    Product Variations
                </a>
                <select name="variants[]" class="filter_select" placeholder="Select variants" multiple>
                    <?php
                    $variants = $db->get('variant_titles',null,"id,variant_title");
                    foreach($variants as $variant){
                        echo "<option ".((in_array($variant['id'],$_GET['variants']))? 'selected' : '')." value='".$variant['id']."'>".$variant['variant_title']."</option>";
                    }
                    ?>
                </select>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    Keywords And Tags
                </a>
                <select name="tags[]" class="filter_select" placeholder="Select tags" multiple>
                    <?php
                    $tags = $db->get('tags',null,"id,name");
                    foreach($tags as $tag){
                        echo "<option ".((in_array($tag['id'],$_GET['tags']))? 'selected' : '')." value='".$tag['id']."'>".$tag['name']."</option>";
                    }
                    ?>
                </select>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                   Country
                </a>
                <select name="country[]" class="filter_select" placeholder="Choose a country" multiple>
                    <?php
                    $countries = $db->get('country_names',null,"id,country_name");
                    foreach($countries as $country){
                        echo "<option ".((in_array($country['id'],$_GET['country']))? 'selected' : '')." value='".$country['id']."'>".$country['country_name']."</option>";
                    }
                    ?>
                </select>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    Region
                </a>
                <select name="region[]" class="filter_select" placeholder="Choose a region" multiple>
                    <?php
                    $regions = $db->get('province_names',null,"id,province_name");
                    foreach($regions as $region){
                        echo "<option ".((in_array($region['id'],$_GET['region']))? 'selected' : '')." value='".$region['id']."'>".$region['province_name']."</option>";
                    }
                    ?>
                </select>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    City
                </a>
                <select name="city[]" class="filter_select" placeholder="Choose a city" multiple>
                    <?php
                    $cities = $db->get('city_names',null,"id,city_name");
                    foreach($cities as $city){
                        echo "<option ".((in_array($city['id'],$_GET['city']))? 'selected' : '')." value='".$city['id']."'>".$city['city_name']."</option>";
                    }
                    ?>
                </select>
            </li>

        </ul>


    </div>
</nav>

  <main id="content" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

    <?php

    if(empty($_SERVER["QUERY_STRING"])){

        $loader = new \Twig\Loader\FilesystemLoader('assets/templates');
        $twig = new \Twig\Environment($loader);

        //Default Content
        $defaultCollections = $db->query("SELECT *, count(product_collections.collection_id) as product_count, UPPER(collections.title) as title FROM product_collections,collections,images WHERE product_collections.collection_id = collections.id AND product_collections.product_id = images.product_id GROUP BY product_collections.collection_id order by product_count DESC LIMIT 100;");
        $defaultCollectionsHTML = $twig->render('default.twig.html', ['defaults' => $defaultCollections]);

        $defaultTags = $db->query("SELECT *, count(product_tags.tag_id) as product_count, UPPER(tags.name) as title FROM images,product_tags,tags WHERE product_tags.product_id = images.product_id AND product_tags.tag_id = tags.id GROUP BY product_tags.tag_id order by product_count DESC LIMIT 100");
        $defaultTagsHTML = $twig->render('default.twig.html', ['defaults' => $defaultTags]);

        $defaultTypes = $db->query("SELECT *, count(product_types.type_id) as product_count, UPPER(types.name) as title FROM product_types,types,images WHERE product_types.product_id = images.product_id AND product_types.type_id = types.id GROUP BY product_types.type_id ORDER BY product_count DESC LIMIT 100;");
        $defaultTypesHTML = $twig->render('default.twig.html', ['defaults' => $defaultTypes]);

        echo "<div class='content_promo' style='color:#444;font-size:35px;'>Welcome to Shomazon!</div>";
        echo "<div class='content_promo'>Search Thousands of Top Online Stores in one Place</div>";

        echo "<div class='content_header'>Choose a Category below, Type a Search query or edit our Filters (Bottom Right) to get Started</div><br>";

        echo "<div class='category_heading'>Product Types</div><hr>";
        echo $defaultTypesHTML;

        echo "<div class='category_heading'>Product Collections</div><hr>";
        echo $defaultCollectionsHTML;

        echo "<div class='category_heading'>Product Tags</div><hr>";
        echo $defaultTagsHTML;
    }



    ?>

</main>


<!--
<nav class="navbar fixed-bottom navbar-expand-sm navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><span class="logo">Shomazon</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <input class="form-control dark form-control-dark w-100" type="text" name="query" placeholder="Search" aria-label="Search">
        <button type="button" class="btn btn-dark nav-item" data-toggle="modal" data-target="#filterModal">Filters</button>
        <?php if(!is_user()){ ?>
                <button type="button" class="btn btn-dark nav-item" data-toggle="modal" data-target="#importModal">Login</button>
            <?php } else { ?>
                <button type="button" class="btn btn-dark nav-item" onclick="logOut();">Logout</button>
        <?php }?>
    </div>
</nav>-->

<!-- Modals -->
<!-- Import Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="importform" action="auth.php" method="POST">
                    <input class="form-control dark form-control-dark w-100" name="shop" type="text" placeholder="Enter Your Store Name" aria-label="Enter Your Store Name">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="import btn btn-primary">Login</button>
            </div>
        </div>
    </div>
</div>
<!-- End Import Modal -->
<!-- End Modals -->



</body>
</html>


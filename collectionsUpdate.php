<?php
require 'vendor/autoload.php';
require 'config.php';
require 'functions.php';
$db = new MysqliDb ('localhost', DB_USER, DB_PASSWORD, DB);
$shops = $db->get("shops");

foreach($shops as $shop){

    $config = array(
        'ShopUrl' => $shop["domain"],
        'AccessToken' => $shop["access_token"],
    );
    $shopify = PHPShopify\ShopifySDK::config($config);
// COUNT SMART COLLECTS
    $collectResource = $shopify->Collect();
    $smart = $shopify->SmartCollection();
    $smarts = $smart->get();
    while ($smarts) {
        foreach ($smarts as $curSmart) {
            $resource = $shopify->Collection($curSmart["id"])->Product();
            $results = $resource->get();

            $count = $count + count($results);

            $saveSmartCollections[$curSmart["id"]]["smartCollection"] = $curSmart;
            $saveSmartCollections[$curSmart["id"]]["products"] = [];
            while ($results) {

                foreach($results as $result){
                    $saveSmartCollections[$curSmart["id"]]["products"][] = $result;
                }

                if (!empty($resource->getNextPageParams())) {
                    $results = $resource->get($resource->getNextPageParams());
                } else {
                    $results = false;
                }
            }
        }
        if (!empty($smart->getNextPageParams())) {
            $smarts = $smart->get($smart->getNextPageParams());
        } else {
            $smarts = false;
        }
    }
    /////
    $collects = $collectResource->get();
    $collections = [];
/////
/// Remove All Collects

    $ids = $db->subQuery();
    $ids->where ("shop_id", $shop['shop_id'], "=");
    $ids->get ("products", null, "product_id");
    $db->where ("product_id", $ids, 'in');
    $res = $db->delete("product_collections");

    //Add Back Collections
    while ($collects) {

        foreach ($collects as $collect) {
            try{
                addCollect($collect);
            }
            catch(Exception $e){
                error_log($e->getMessage());
            }
        }

        if (!empty($collectResource->getNextPageParams())) {
            $collects = $collectResource->get($collectResource->getNextPageParams());
        } else {
            $collects = false;
        }
    }
    foreach ($saveSmartCollections as $key => $smartCollection) {
        // Lookup Collection By Name
        $db->where("title", strtolower($smartCollection["smartCollection"]["title"]));
        $collectionDB = $db->getOne("collections");
        if (empty($collectionDB)) {
            $id = $db->insert("collections", array(
                "title" => strtolower($smartCollection["smartCollection"]["title"]),
                "handle" => strtolower($smartCollection["smartCollection"]["handle"])
            ));
        } else {
            $id = $collectionDB["id"];
        }

        foreach ($smartCollection["products"] as $product) {
            $db->insert("product_collections",
                array(
                    "product_id" => $product["id"],
                    "collection_id" => $id
                ));
        }
    }
}//foreach shops as shop

